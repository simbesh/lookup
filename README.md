# lookUPtx

![](public/preview.png)

### Hosted Demo
https://lookuptx.netlify.app

### Get a token
Create a token [Here](https://api.up.com.au/getting_started)

### Create a local instance:

Install [Nodejs](https://nodejs.org/)

Clone repo
```
git clone https://gitlab.com/simbesh/lookup.git
```
Install dependencies
```
npm install
```
Run it
```
npm run start
```