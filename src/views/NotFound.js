import React from 'react';
import notFoundImage from '../images/not-found.png'

const NotFound = () => {
    return (
        <React.Fragment>
            <div style={{
                display: "flex",
                justifyContent: "center"
            }}>
                <img src={notFoundImage}/>

            </div>
            <div style={{
                display: "flex",
                justifyContent: "center"
            }}>
                <div>NOT FOUND</div>
            </div>
        </React.Fragment>
    )
}


export default NotFound;