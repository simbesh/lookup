import React, {useEffect, useState} from 'react';
import axios from 'axios';
import Up from "../services/httpClient"
import Constants from "../Constants"

const Token = ({token, raiseClearToken, raiseToken, raiseActive}) => {

    const [accounts, setAccounts] = useState([]);
    const [tokenInputValue, setTokenInputValue] = useState("");
    const [feebackMain, setFeedbackMain] = useState("");
    const [feedbackText, setFeedbackText] = useState([]);
    const [checkboxChecked, setCheckboxChecked] = useState(false);
    const [checkboxClass, setCheckboxClass] = useState("checkbox");
    // const [saveDisabled, setSaveDisabled] = useState("button-disabled");


    useEffect(() => {
        raiseActive(Constants.TOKEN_PAGE)
    }, []);

    useEffect(() => {
        setTokenInputValue(token)
        setCheckboxChecked(Boolean(token))
    }, [token]);

    useEffect(() => {
        const newClass = checkboxChecked ? "checkbox checkbox-checked" : "checkbox"
        setCheckboxClass(newClass)
    }, [checkboxChecked]);

    const handleTest = async () => {
        if (tokenInputValue === "") {
            setFeedbackMain("🤦‍♂️")
            setFeedbackText(["Paste your token in the text box."])
        } else {
            try {
                let res = await Up.ping(tokenInputValue)
                if (res?.status === 200) {
                    handleSaveToken(tokenInputValue)
                    setFeedbackMain("👍")
                    setFeedbackText(["Good to go."])
                }
            } catch (e) {
                console.error("ERROR:", e?.response?.data?.errors[0])
                setFeedbackMain("👎")
                if (e?.response?.status === 401) {

                    setFeedbackText(["401 Unauthorised", "Up rejected the token."])
                }
            }
        }
    }
    const handleInput = ({target: input}) => {
        setTokenInputValue(input.value.trim())
    }

    const handleHelp = () => {
        const text = [
            "To use this tool you must input your up api token.",
            " ",
            "This open source tool runs as a/an SPA",
            "Your token only exists on your device and is never sent anywhere except to up's servers for authorisation.",
            " ",
            "Click the link bellow 👇 to create one.",
            " ",
            "Paste it above and click test, that's it."
        ]
        setFeedbackText(text)
        setFeedbackMain("")
    }

    const handleClear = () => {
        raiseClearToken()
        setFeedbackMain("💨")
        setFeedbackText(["Deleted."])
    };

    const handleSaveToken = newToken => {
        raiseToken(newToken, checkboxChecked)
    }

    return (
        <React.Fragment>
            <div className={"token-content"}>
                <div className={"token-input"}>
                    <input value={tokenInputValue || ""} onChange={handleInput}
                           placeholder={"up:yeah:thisISwhatYOURtokenSHOULDlookLIKE"}/>
                </div>
                <div className={"token-button-inputs"}>
                    <div className={"help-button-div"}>
                        <button className={"help-button"} onClick={handleHelp}>Help</button>
                        <button className={"help-button"} onClick={handleClear}>Clear Token</button>
                    </div>
                    <div className={"text-label"}>Remember token</div>
                    <button className={checkboxClass} onClick={() => setCheckboxChecked(!checkboxChecked)}/>
                    <button onClick={handleTest}>Test</button>
                </div>
                <div className={"token-feedback"}>
                    <div>
                        {feebackMain}
                    </div>
                </div>
                <div className={"token-helper-text"}>
                    {feedbackText.length > 0 && (feedbackText.map((text, i) => (
                        <div key={i}>
                            {text}
                        </div>
                    )))}
                </div>
                <div className={"nav-link footer"} >
                    <a href='https://api.up.com.au/getting_started' target="_blank"
                        className={"nav-link-active"}
                       >Create token here</a>
                </div>

            </div>
        </React.Fragment>
    )
}


export default Token;