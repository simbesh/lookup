import React, {useEffect, useState} from 'react';
import Up from "../services/httpClient"
import Constants from "../Constants";
import TransactionCard from "../components/TransactionCard";
import {useHistory, useParams} from 'react-router-dom';
import LookLoader from "../components/LookLoader"

const Accounts = ({token, raiseActive}) => {
    const history = useHistory();
    let {account_id} = useParams();

    const [accounts, setAccounts] = useState([]);
    const [transactionsData, setTransactionsData] = useState({});
    const [activeAccountId, setActiveAccountId] = useState(account_id);
    const [loading, setLoading] = useState(false);

    async function getAccounts() {
        setLoading(true)
        try {
            let res = await Up.getAccounts(token)
            if (res?.data?.data.length) {
                setAccounts(res.data.data)
            }
        } catch (e) {
            console.error("e:", e)
        }
        setLoading(false)
    }

    useEffect(() => {
        raiseActive(Constants.ACCOUNTS_PAGE)
    }, []);

    useEffect(() => {
        if (token && accounts.length === 0) {
            getAccounts()
            if (activeAccountId) handleGetInitialTransactions(activeAccountId)
        }
    }, [token]);

    function formatCurrency(value) {
        let [dollars, cents] = value.split(".")
        dollars = dollars.split("")

        if (dollars.length >= 5) {
            dollars.splice(dollars.length - 3, 0, ",")
        }
        if (dollars.length >= 7) {
            dollars.splice(dollars.length - 7, 0, ",")
        }

        return dollars.join("") + "." + cents
    }

    const handleGetInitialTransactions = async (id) => {
        setActiveAccountId(id)
        let newTransactions = {...transactionsData}

        if (!newTransactions[id]?.txs.length) {
            setLoading(true)
            try {
                let res = await Up.getTransactions(token, id)
                if (res?.data?.data.length) {
                    newTransactions[id] = {
                        txs: res?.data?.data,
                        nextLink: res?.data?.links.next
                    }
                    setTransactionsData(newTransactions)
                }
            } catch (e) {

            }
            setLoading(false)
        }
        history.push("/accounts/" + id)
    }

    const fetchMoreTransactions = async (pageSize = 20) => {
        let nextLink = transactionsData[activeAccountId].nextLink
        if (nextLink) {
            setLoading(true)
            try {
                let res = await Up.getMoreTransactions(token, nextLink, pageSize)
                if (res?.data?.data.length) {
                    let newTransactions = {...transactionsData}
                    newTransactions[activeAccountId].txs = newTransactions[activeAccountId].txs.concat(res?.data?.data)
                    newTransactions[activeAccountId].nextLink = res?.data?.links.next

                    setTransactionsData(newTransactions)
                }
            } catch (e) {

            }
            setLoading(false)
        }
    }

    let newDay = null
    let newMonth = null
    let newMonthString = ""
    let newDayString = ""

    const getNewDay = (data) => {
        const formatDate = date => {
            const month = date.toLocaleString('default', {month: 'short'});
            const day = date.toLocaleString('default', {weekday: 'short'});
            return day + " " + date.getDate() + " " + month
        }
        const sameDay = (date1, date2) => {
            return date1.getFullYear() === date2.getFullYear()
                && date1.getMonth() === date2.getMonth()
                && date1.getDate() === date2.getDate()
        }

        let createdDate = new Date(data.attributes.createdAt)
        if (!newDay || !sameDay(createdDate, newDay)) {
            newDay = createdDate
            newDayString = formatDate(createdDate)
            return formatDate(createdDate)
        } else {
        }
        return null
    }

    const getNewMonth = (data) => {
        const formatDate = date => {
            return date.toLocaleString('default', {month: 'long'});
        }
        const sameMonth = (date1, date2) => {
            return date1.getFullYear() === date2.getFullYear()
                && date1.getMonth() === date2.getMonth()
        }

        let createdDate = new Date(data.attributes.createdAt)
        if (!newMonth || !sameMonth(createdDate, newDay)) {
            newMonth = createdDate
            newMonthString = formatDate(createdDate)
            return formatDate(createdDate)
        } else {
        }
        return null
    }

    let transactionsNotEmpty = activeAccountId && transactionsData[activeAccountId]?.txs

    return (
        <React.Fragment>
            {token && (
                <React.Fragment>
                    <div className={"flex-container mini-margin-top"}>
                        {accounts.map(({attributes, id}, index) => (
                            <div key={index} className={"card pointer " + (activeAccountId === id ? "card-active" : "")}
                                 id={id} onClick={() => handleGetInitialTransactions(id)}>
                                <div className={"text-label-medium"}>{attributes.displayName}</div>
                                <div className={"text-label-xsmall primaryColor"}>{attributes.accountType}</div>
                                <br/>
                                <div className={"text-label"}>{"$" + formatCurrency(attributes.balance.value)}</div>
                            </div>
                        ))}
                    </div>
                    <div className={"transactions-container"}>
                        {transactionsNotEmpty && (
                            <div className={"flex-container"}>
                                <p>WIP Tip: click on a tx row to see the full tx data in the console</p>
                            </div>
                        )}
                        {transactionsNotEmpty && transactionsData[activeAccountId].txs.map((tx, i) =>
                            <React.Fragment key={i}>
                                {Boolean(getNewMonth(tx)) && (<div className={"month-card"}>{newMonthString}</div>)}
                                {Boolean(getNewDay(tx)) && (<div className={"date-card"}>{newDayString}</div>)}
                                <TransactionCard key={i} data={tx}/>
                            </React.Fragment>
                        )}
                    </div>
                    <LookLoader hidden={!loading}/>
                    <div className={"flex-center med-margin-top med-margin-bottom"}>
                        {transactionsNotEmpty && !loading &&(
                            <button onClick={() => fetchMoreTransactions()}>Get More Txs ></button>
                        )}
                    </div>
                </React.Fragment>
            )}
            {!token && (
                <div className={"text-label flex-center mega-margin-top"}>
                    <div>Token is missing 😢</div>
                </div>
            )}
        </React.Fragment>
    )
}


export default Accounts;