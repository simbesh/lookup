import React, {useEffect} from 'react';
import gitlabLogo from '../images/gitlab.svg';
import Constants from "../Constants";

const Transactions = ({token, raiseActive}) => {

    useEffect(() => {
        raiseActive(Constants.TRANSACTIONS_PAGE)
    }, []);

    return (
        <React.Fragment>
            <div>Transations</div>
        </React.Fragment>
    )
}


export default Transactions;