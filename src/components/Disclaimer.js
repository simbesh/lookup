import React from 'react';
import gitlabLogo from '../images/gitlab.svg';
import paintLogo from '../images/paint-logo.png'

const Disclaimer = (props) => {
    return (
        <React.Fragment>
            {!props.hidden && (<div className={"disclaimer-hide"} style={{
                backgroundColor: "#ffef6e",
                color: "#242430",
                display: "flex",
                justifyContent: "center",
                fontSize: "1.2rem",
                // fontWeight: "bolder"
            }}>
                <div style={{padding: "12px 0"}}>This unofficial site has NO affiliation with UP Bank.</div>
                <button onClick={() => props.raiseDismiss()}>X</button>
            </div>)}
        </React.Fragment>
    )
}


export default Disclaimer;