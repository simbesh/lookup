import React from "react";
import lookup from "../images/lookup.gif"

const LookLoader = ({hidden}) => {
    return (
        <React.Fragment>
            {!hidden && (
                <div className="flex-center mt-48">
                    <div>
                        <img src={lookup} alt="lookup-loader" width="96"/>
                    </div>
                </div>
            )}
        </React.Fragment>
    )
}

export default LookLoader
