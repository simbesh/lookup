import React, {useEffect} from 'react';
import gitlabLogo from '../images/gitlab.svg';
import Constants from "../Constants";

const TransactionCard = ({i, data}) => {
    const [formattedValue, colour] = formatValue(data)
    const tags = getTags(data)
    const txTime = getTime(data)

    return (
        <div key={i} className={"transaction-card"} onClick={() => console.log(JSON.stringify(data, null, 2))}>
            <div style={{marginLeft: "0%"}}>
                {txTime}
            </div>
            <div style={{marginLeft: "10%"}}>
                {data.attributes.description}
            </div>
            {tags.map((tag, i) => (
                <div key={i} className={"tx-tag"}>{tag}</div>
            ))}
            <div  style={{color: colour, marginLeft: "auto", marginRight: "10%"}}>
                {formattedValue}
            </div>
            <br/>
        </div>
    )
}

const formatValue = data => {
    let valueInBaseUnits = data.attributes.amount.valueInBaseUnits
    let isPositiveValue = valueInBaseUnits >= 0
    let colour = "#242430"
    let formattedValue = (valueInBaseUnits / 100).toFixed(2)

    if (isPositiveValue) {
        colour = "#0cb682"
        formattedValue = "+$" + formattedValue
    } else {
        formattedValue = "$" + Math.abs(formattedValue).toFixed(2)
    }

    return [formattedValue, colour]
}

const getTime = data => {
    const date = new Date(data.attributes.createdAt)
    let hours = date.getHours()

    let minutes = date.getMinutes()
    minutes = minutes < 10 ? "0" + minutes : minutes

    return (hours > 12 ? hours - 12 : hours) + ":" + minutes + (hours > 11 ? "pm" : "am")
}

const getTags = data => {
    return data.relationships.tags.data.length ? data.relationships.tags.data.map(tag => tag.id) : []
}


export default TransactionCard;