import React, {useEffect} from 'react';
import gitlabLogo from '../images/gitlab.svg';
import paintLogo from '../images/logo180.png'
import {NavLink} from "react-router-dom";
import {Link} from "react-router-dom";
import Constants from "../Constants";

const Header = ({navSelected}) => {

    return (
        <React.Fragment>
            <div style={{position: "relative"}} className={"header"}>
                <div style={{margin: "12px 24px", position: "absolute", top: 0, right: 0}}>
                    <a className={"gitlab-link"} href="https://gitlab.com/simbesh/lookup" target="_blank">
                        <img src={gitlabLogo} width="48px" height="48px"/>
                    </a>
                </div>
                <div style={{
                    backgroundColor: "#ff7a64",
                    color: "#242430",
                    display: "flex",
                    justifyContent: "center",
                    fontSize: "3.8rem",
                    fontWeight: "bolder"
                }}>
                    {/*<div style={{padding: "32px 0"}}>Look</div>*/}
                    <div><img src={paintLogo} width="128" style={{position: "relative", bottom: 0}}/></div>
                    <div style={{padding: "32px 0"}}>your transactions</div>
                </div>
                <div className={"nav-link-container"}>
                    <div className={"nav-link" + (navSelected === Constants.TOKEN_PAGE ? " nav-link-active" : "")}>
                        <Link to="/token" >Token</Link>
                    </div>
                    <div className={"nav-link" + (navSelected === Constants.ACCOUNTS_PAGE ? " nav-link-active" : "")}>
                        <Link to="/accounts" >Accounts</Link>
                    </div>
                    {/*<div className={"nav-link" + (navSelected === Constants.TRANSACTIONS_PAGE ? " nav-link-active" : "")}>*/}
                    {/*    <Link to="/transactions">Transactions</Link>*/}
                    {/*</div>*/}
                </div>
            </div>
        </React.Fragment>
    )
}


export default Header;