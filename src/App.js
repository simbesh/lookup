import React, {useEffect, useState} from 'react';
import {
    BrowserRouter as Router,
    Route,
    Switch,
    Redirect
} from "react-router-dom";

import Header from "./components/Header";
import Disclaimer from "./components/Disclaimer";
import Accounts from "./views/Accounts";
import Transactions from "./views/Transactions";
import NotFound from "./views/NotFound";
import Token from "./views/Token";


import Constants from "./Constants"

const disclaimerFlagKey = 'dismissed-disclaimer'
const upTokenKey = 'upToken'

function App() {
    const [navSelected, setNavSelected] = useState(null);
    const [token, setToken] = useState(null);
    const [showDisclaimer, setShowDisclaimer] = useState(false);

    useEffect(() => {
        try {
            if (!localStorage.getItem(disclaimerFlagKey)) {
                setShowDisclaimer(true)
            }
        } catch (e) {
        }
        try {
            const token = localStorage.getItem(upTokenKey)
            if (token) {
                setToken(token)
            }
        } catch (e) {
        }
    }, []);

    const handleActivePageChange = page => {
        setNavSelected(page)
    }

    const handleDismiss = () => {
        localStorage.setItem(disclaimerFlagKey, "true")
        setShowDisclaimer(false)
    }

    const handleClearToken = () => {
        setToken("")
        localStorage.removeItem(upTokenKey)
    }

    const handleSaveToken = (token, saveToLocalstorage = false) => {
        setToken(token)
        if (saveToLocalstorage) {
            localStorage.setItem(upTokenKey, token)
        }
    }

    return (
        <div className="App page">
            <Router>
                <Disclaimer raiseDismiss={() => handleDismiss()} hidden={!showDisclaimer}/>
                <Header navSelected={navSelected}/>
                <div className={"content"}>
                    <Switch>
                        <Route path="/token">
                            <Token
                                token={token}
                                raiseClearToken={handleClearToken}
                                raiseToken={handleSaveToken}
                                raiseActive={(page) => handleActivePageChange(page)}
                            />
                        </Route>
                        <Route path="/accounts/:account_id">
                            <Accounts
                                token={token}
                                raiseActive={(page) => handleActivePageChange(page)}
                            />
                        </Route>
                        <Route path="/accounts" exact>
                            <Accounts
                                token={token}
                                raiseActive={(page) => handleActivePageChange(page)}
                            />
                        </Route>
                        <Route path="/transactions">
                            <Transactions
                                token={token}
                                raiseActive={(page) => handleActivePageChange(page)}
                            />
                        </Route>
                        <Route path="/not-found">
                            <NotFound
                                token={token}
                                raiseActive={(page) => handleActivePageChange(page)}
                            />
                        </Route>
                        <Redirect from="/" exact to="/token"/>
                        <Redirect to="/not-found"/>
                    </Switch>
                </div>
            </Router>
        </div>
    );
}

export default App;
