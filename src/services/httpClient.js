import axios from 'axios';

const baseURL = "https://api.up.com.au/api/v1"

const get = async (token, endpoint) => {
        return await axios.get(baseURL + endpoint, {headers: {Authorization: 'Bearer ' + token}})

}
const ping = async token => {
    const endpoint = "/util/ping"
    return await get(token, endpoint)

}
const getAccounts = async (token, endpoint = "/accounts") => {
    return await get(token, endpoint)
}
const getTransactions = async (token, id, pageSize = 20) => {
    const endpoint = "/accounts/" + id + "/transactions/" + encodeURI("?page[size]=") + pageSize
    return await get(token, endpoint)
}
const getMoreTransactions = async (token, endpoint, pageSize = 20) => {
    endpoint = endpoint.replace(baseURL, "")
    return await get(token, endpoint)
}

export default {
    ping,
    getAccounts,
    getTransactions,
    getMoreTransactions
}